# rust-geo-wkt-writer

A trait for geo-types Geometries to output a WKT representation.

## ToWkt

This trait applies to all Geometry types in geo-types. There is no Rect or Triangle WKT text type, so both of those will be serialized as POLYGON WKT representations. Any polygons returned will be normalized.

### Examples

```rust
use geo_types::{Geometry, GeometryCollection, polygon, point};
use geo_wkt_writer::ToWkt;

let poly = Geometry::Polygon(polygon![
            (x: 1.0, y: 1.0),
            (x: 4.0, y: 1.0),
            (x: 4.0, y: 4.0),
            (x: 1.0, y: 4.0),
            (x: 1.0, y: 1.0),
        ]);
let pe = Geometry::Point(point!(x: 1.0, y: 1.0));
let gc = GeometryCollection(vec![pe, poly]);
let wkt_out = gc.to_wkt();
let expected = String::from("GEOMETRYCOLLECTION(POINT(1 1),POLYGON((1 1,4 1,4 4,1 4,1 1)))");
assert_eq!(wkt_out, expected);
```